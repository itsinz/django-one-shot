from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/todolist.html", context)
